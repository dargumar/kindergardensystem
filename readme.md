## Test JAVA Task -- M.D

- METHOD "GET", URL: http://localhost:4567/api/kindergarten/ - List of kindergartens
- METHOD "GET", URL: http://localhost:4567/api/kindergarten/:id - kindergarten by ID
- METHOD "GET", URL: http://localhost:4567/api/kindergarten/:id/children/ - List of children in kindergarten specified

- METHOD "POST", URL: http://localhost:4567/api/kindergarten/:id/children/remove - removes child from the que.
Request Body: {"ssCode":"XXXXXX-XXXXX"}

- METHOD "POST", URL: http://localhost:4567/api/kindergarten/:id/children/attach - adds child to the que.
Request Body: {"ssCode":"XXXXXX-XXXXX", "legacy":"true"}
