package com.zz;

import org.junit.Test;

import junit.framework.Assert;

public class StorageTest {

	@Test
	public void test() {
		Storage.loadData();

		Assert.assertEquals(Storage.applications.size(), 8);
		Assert.assertNotNull(Storage.applications.get(0));

		Assert.assertEquals(Storage.children.size(), 3);
		Assert.assertNotNull(Storage.children.get(0));

		Assert.assertEquals(Storage.kindergartens.size(), 10);
		Assert.assertNotNull(Storage.kindergartens.get(0));
		
		Assert.assertEquals(Storage.kindergartens.get(4).getQueLength(), 3);
	}
}
