package com.zz;

import org.junit.Test;

import junit.framework.Assert;

public class ChildTest {

	@Test
	public void test() {
		Child child = new Child("Test", "User", "123456-12345");

		Assert.assertEquals(child.getName(), "Test");
		Assert.assertEquals(child.getSurname(), "User");

		Assert.assertEquals(child.getSsCode(), "123456-12345");
		Assert.assertEquals(child.isSsCodeValid(), true);
		child.setSsCode("123123123");
		Assert.assertEquals(child.isSsCodeValid(), false);
	}

}
