package com.zz;

import org.junit.Test;

import junit.framework.Assert;

public class KindergartenHandlerTest {

	public KindergartenHandlerTest() {
		Storage.loadData();
	}

	@Test
	public void dataLoadedTest() {
		Assert.assertNotNull(KindergartenHandler.getKindergardenByID("1"));
	}
	
	
	@Test
	public void addChildTest() {
		Kindergarten k = Storage.kindergartens.get(3);
		Child applicant = k.applications.get(0).applicant;
		Assert.assertTrue(KindergartenHandler.removeChildFromQue("3", applicant));
		Assert.assertTrue(KindergartenHandler.addChildToQue("3", "{\"ssCode\":" + applicant.getSsCode() + ",\"legacy\":\"false\"}"));
	}

	@Test
	public void responseFormatTest() {
		Assert.assertEquals(KindergartenHandler.packResponse("a"), "{\"data\":a}");
	}

}
