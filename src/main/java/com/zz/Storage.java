package com.zz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.google.gson.Gson;


/**
 * 
 * @author martins.darguzis@gmail.com
 * Storage is used to load demo data and simulate what normally would be done with sql requests
 *
 */
public class Storage {

	public static List<Child> children;
	public static List<Kindergarten> kindergartens;
	public static List<Application> applications;
	
	private final static String childrenDataFile = "./src/main/resources/children.txt";
	private final static String kindergardenDataFile = "./src/main/resources/kindergarden.txt";
	private final static String kindergardenApplicationFile = "./src/main/resources/childApplications.txt";

	public static void loadData() {
		children = loadChildrenFromFile();
		applications = loadApplications();
		kindergartens = loadKindergartensFromFile();
	}

	private static List<Child> loadChildrenFromFile() {
		List<Child> children = new ArrayList<>();
		File file = new File(childrenDataFile); 
		Scanner sc;
    	
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				Child child = new Gson().fromJson(sc.nextLine(), Child.class);
				children.add(child);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return children;
	}

	private static List<Kindergarten> loadKindergartensFromFile() {
		List<Kindergarten> kindergardens = new ArrayList<>();
		File file = new File(kindergardenDataFile); 
		Scanner sc;

		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				Kindergarten k = new Gson().fromJson(sc.nextLine(), Kindergarten.class);
				k.applications = readKindergardenApplications(k.getID());
				k.setQueLength(k.applications.size());
				kindergardens.add(k);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return kindergardens;
	}

	public static List<Application> loadApplications(){
		List<Application> aplList = new ArrayList<>();
		File file = new File(kindergardenApplicationFile); 
		Scanner sc;
    	
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				Application apl = new Gson().fromJson(sc.nextLine(), Application.class);
				apl.applicant = Child.getByID(apl.getChildID());
				aplList.add(apl);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return aplList;
	}

	public static List<Application> readKindergardenApplications(String id) {
		return applications.stream()
						   .filter(a -> id.equals(a.getKindergartenID()))
						   .collect(Collectors.toList());
	}
}
