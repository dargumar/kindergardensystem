package com.zz;

import static spark.Spark.*;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * 
 * @author martins.darguzis@gmail.com
 * @version 1.0
 * @category Java Test task
 * 
 * RoutherHandler is supposed to route user requests.
 *
 */

public class RouteHandler{
    public static void main(String[] args) {
    	Storage.loadData();
    	path("/api", () -> {
    	    before("/*", (q, a) -> System.out.println("Received api call"));
    	    path("/kindergarten", () -> {

    	    	get("/", (request, response) -> {
    	    		response.status(HttpServletResponse.SC_OK);
    	    		response.type("application/json");
        	    	return KindergartenHandler.listKindergardens();
        	    });

    	    	get("/:id", (request, response) -> {
    	    		response.status(HttpServletResponse.SC_OK);
    	    		response.type("application/json");
        	    	return KindergartenHandler.getKindergardenByID(request.params(":id"));
        	    });

    	    	get("/:id/children/", (request, response) -> {
    	    		response.status(HttpServletResponse.SC_OK);
    	    		response.type("application/json");
        	    	return KindergartenHandler.getKindergardenApplicants(request.params(":id"));
        	    });
    	    	
    	    	post("/:id/children/remove", (request, response) -> {
    	    		response.type("application/json");
    	    		Child child = new Gson().fromJson(request.body(), Child.class);
        	    	if (KindergartenHandler.removeChildFromQue(request.params(":id"), child)) {
        	    		response.status(HttpServletResponse.SC_OK);
        	    	}else {
        	    		response.status(HttpServletResponse.SC_BAD_REQUEST);
        	    	}
        	    	return "";
        	    });

    	    	post("/:id/children/attach", (request, response) -> {
    	    		response.type("application/json");
    	    		if (KindergartenHandler.addChildToQue(request.params(":id"), request.body())) {
        	    		response.status(HttpServletResponse.SC_OK);
        	    	}else {
        	    		response.status(HttpServletResponse.SC_BAD_REQUEST);
        	    	}
        	    	return "";
        	    });
    	    });
    	});
    }
}
