package com.zz;

import java.util.Arrays;

import com.google.gson.Gson;

public class KindergartenHandler {
	public static String listKindergardens() {
	    return packResponse(Arrays.toString(Kindergarten.list()));
	}

	public static String getKindergardenByID(String id) {
		return packResponse(Kindergarten.findByID(id).toString());
	}

	public static String getKindergardenApplicants(String id) {
		Kindergarten k = Kindergarten.findByID(id);
		return packResponse(k.listApplicants());
	}

	public static boolean removeChildFromQue(String id, Child child) {
		if(!child.isSsCodeValid()) {
			return false;
		}
	
		child = child.getBySsCode();
		if(child == null){
			return false;
		}
		Kindergarten k = Kindergarten.findByID(id);
		return k.removeApplicant(child);
	}

	public static boolean addChildToQue(String id, String requestBody) {
		Child child = new Gson().fromJson(requestBody, Child.class);
		if(!child.isSsCodeValid()) {
			return false;
		}

		Application apl = new Gson().fromJson(requestBody, Application.class);

		child = child.getBySsCode();
		if(child == null || id == ""){
			return false;
		}
		Kindergarten k = Kindergarten.findByID(id);
		return k.addApplicant(child, apl);
	}

	public static String packResponse(String body) {
		return "{\"data\":" + body + "}"; // Not the best, but I'm running out of time.
	}
}
