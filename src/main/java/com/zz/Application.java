package com.zz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class Application {
    @SerializedName("applicationDate")
	private String date;

    @SerializedName("kindergardenID")
	private String kindergartenID;

    @SerializedName("childID")
    private String childID;
    
    @SerializedName("legacy")
    private boolean legacy;

    public Child applicant;
    
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");

    public void setDate() {
		this.date = formatter.format(new Date());
    }

	public Date getDate() {
	    try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		return null;
	}

	public String getKindergartenID() {
		return kindergartenID;
	}

	public void setKindergartenID(String id) {
		this.kindergartenID = id;
	}

	public String getChildID() {
		return childID;
	}


	public boolean isLegacy() {
		return legacy;
	}

	public void setLegacy(boolean legacy) {
		this.legacy = legacy;
	}

	@Override
	public String toString() {
		return "{\"date\":\"" + date + "\",\"legacy\":\"" + legacy + "\",\"applicant\":" + applicant + "}";
	}
}
