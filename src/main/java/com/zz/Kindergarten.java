package com.zz;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Kindergarten is used as POJO-like class - since no DB is used - hibernate is substituted with "spark"
 *
 */
public class Kindergarten {
    @SerializedName("id")
	private String id;

    @SerializedName("indicator")
	private String indicator;

    @SerializedName("title")
    private String title;
	
    @SerializedName("address")
    private String address;

    @SerializedName("length")
    private int queLength;

    public List<Application> applications;
	
	public Kindergarten(String indicator, String title, String address, int queLength) {
		super();
		this.indicator = indicator;
		this.title = title;
		this.address = address;
		this.queLength = queLength;
	}

	public static Kindergarten findByID(String id) {
		return Storage.kindergartens.stream()
				      .filter(k -> id.equals(k.getID())).findAny().orElse(null);
	}

	public static Kindergarten[] list() {
		Kindergarten[] array = new Kindergarten[Storage.kindergartens.size()];
		Storage.kindergartens.toArray(array);
		return array;
	}

	public String listApplicants() {
		String[] outputstrings = new String[this.applications.size()];
		List<Application> sortedList = sortedList();

		for(int i=0; i<this.applications.size(); i++) {
			outputstrings[i] = sortedList.get(i).toString();
		}
		return "[" + String.join(",",outputstrings) + "]";
	}

	private List<Application> sortedList() {
		Collections.sort(this.applications, new Comparator<Application>() {
			@Override
			public int compare(Application o1, Application o2) {
				 o1.getDate().compareTo(o2.getDate());
				 return Boolean.compare(o2.isLegacy(), o1.isLegacy());
			}
	    });
		return this.applications;
	}

	public int findApplication(Child child) {
		for(int i=0; i< this.applications.size(); i++) {
			if(this.applications.get(i).applicant.getID() == child.getID()) {
				return i;
			}
		}
		return -1;
	}

	public boolean removeApplicant(Child child) {
		this.applications.remove(findApplication(child));
		return true;
	}

	public boolean addApplicant(Child child, Application apl) {
		if(findApplication(child)==-1) {
			return true;
		}
		apl.applicant = child;
		apl.setKindergartenID(this.id);
		apl.setDate();
		
		this.applications.add(apl);
		return true;
	}

	
	public String getID() {
		return id;
	}

	public String getIndicator() {
		return indicator;
	}

	public String getTitle() {
		return title;
	}

	public String getAddress() {
		return address;
	}

	public int getQueLength() {
		return queLength;
	}

	public void setQueLength(int queLength) {
		this.queLength = queLength;
	}

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"indicator\":\"" + indicator + "\",\"title\":\"" + title
				+ "\",\"address\":\"" + address	+ "\",\"queLength\":\"" + queLength + "\"}";
	}

}
