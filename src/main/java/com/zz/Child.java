package com.zz;

import com.google.gson.annotations.SerializedName;

public class Child{
    @SerializedName("id")
    private String id;
	
    @SerializedName("name")
    private String name;
	
    @SerializedName("surname")
    private String surname;
	
    @SerializedName("ssCode")
    private String ssCode;

    private final String ssCodeRegex= "[0-9]{6}-[0-9]{5}";
	public Child(String name, String surname, String ssCode) {
		super();
		this.name = name;
		this.surname = surname;
		this.ssCode = ssCode;
	}

	public static Child getByID(String id) {
		return Storage.children.stream()
			      .filter(k -> id.equals(k.getID())).findAny().orElse(null);
	}
	public boolean isSsCodeValid() {
		return this.ssCode.matches(ssCodeRegex);
	}

	public Child getBySsCode() {
		return Storage.children.stream()
			          .filter(k -> this.ssCode.equals(k.getSsCode())).findAny().orElse(null);
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getSsCode() {
		return ssCode;
	}

	public void setSsCode(String ssCode) {
		this.ssCode = ssCode;
	}
	
	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"name\":\"" + name + "\",\"surname\":\""
	           + surname + "\",\"ssCode\":\"" + ssCode + "\"}";
	}
	
}
